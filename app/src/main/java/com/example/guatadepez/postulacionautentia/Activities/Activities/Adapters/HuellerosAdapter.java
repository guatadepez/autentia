package com.example.guatadepez.postulacionautentia.Activities.Activities.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.guatadepez.postulacionautentia.Activities.Activities.Classes.Huellero;
import com.example.guatadepez.postulacionautentia.Activities.Activities.DetailActivity;
import com.example.guatadepez.postulacionautentia.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HuellerosAdapter extends RecyclerView.Adapter<HuellerosAdapter.MyViewHolder> {

    private List<Huellero> huelleroList;
    private Context ctx;
    String uid;




    public HuellerosAdapter(Context context, List<Huellero> matchList,String uid) {
        this.huelleroList = matchList;
        this.ctx = context;
        this.uid = uid;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView huellero_name,huellero_vendorID,huellero_productID;
        ImageButton huellero_image;
        CardView cv;

        public MyViewHolder(View itemView) {
            super(itemView);

            this.huellero_name = itemView.findViewById(R.id.nombre);
            this.huellero_vendorID = itemView.findViewById(R.id.vendorid);
            this.huellero_productID = itemView.findViewById(R.id.productid);
            this.huellero_image = itemView.findViewById(R.id.imagen);
            this.cv =  itemView.findViewById(R.id.huellero_id);
        }
    }

    @NonNull
    @Override
    public HuellerosAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.huellero_card,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HuellerosAdapter.MyViewHolder holder, int position) {

        final Huellero huellero = huelleroList.get(position);

        holder.huellero_name.setText(huellero.getName());
        holder.huellero_productID.setText(huellero.getProductID());
        holder.huellero_vendorID.setText(huellero.getVendorID());
        Picasso.with(ctx).load(huellero.getImageURL()).into(holder.huellero_image);

        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, DetailActivity.class);
                intent.putExtra("name",huellero.getName());
                intent.putExtra("uid",huellero.getProductID());
                ctx.startActivity(intent);
            }
        });


    }
    @Override
    public int getItemCount() {
        return huelleroList.size();
    }
}
