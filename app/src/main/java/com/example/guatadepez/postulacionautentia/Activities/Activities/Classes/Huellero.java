package com.example.guatadepez.postulacionautentia.Activities.Activities.Classes;

public class Huellero {

    private String Name;
    private String VendorID;
    private String ProductID;
    private String imageURL;

    public Huellero (String Name,String VendorID,String ProductID,String imgURL){

        this.Name = Name;
        this.VendorID = VendorID;
        this.ProductID = ProductID;
        this.imageURL = imgURL;
    }

    public String getName(){return Name;}

    public String getProductID() {
        return ProductID;
    }

    public String getVendorID() {
        return VendorID;
    }

    public String getImageURL() {
        return imageURL;
    }
}
