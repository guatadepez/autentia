package com.example.guatadepez.postulacionautentia.Activities.Activities;

import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.guatadepez.postulacionautentia.R;
import android.os.CountDownTimer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SplashScreenActivity extends AppCompatActivity {

    String url= "http://qa.autentia.cl/test_biometria_movil.php/getUid";
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        final RequestQueue queue = Volley.newRequestQueue(this);


        // Splash screen timer

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                            url, null,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {

                                    //Success Callback
                                    try {
                                        Caller(response.getString("uid"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    //Failure Callback

                                }
                            })
                    {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Authorization", "4f206d3636e3fa50322b1a89dd50805bf8f056f5");

                            return params;
                        }
                    };

                    queue.add(jsonObjReq);
                    finish();
                }
            }, SPLASH_TIME_OUT);



    }


    void Caller(String uid) {

        Log.d("Uid",uid);
        Intent i = new Intent(SplashScreenActivity.this,HuellerosActivity.class);
        i.putExtra("uid",uid);
        startActivity(i);

    }

}
