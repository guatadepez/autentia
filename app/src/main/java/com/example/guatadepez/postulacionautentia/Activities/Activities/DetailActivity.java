package com.example.guatadepez.postulacionautentia.Activities.Activities;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.example.guatadepez.postulacionautentia.Activities.Activities.Adapters.HuellerosAdapter;
import com.example.guatadepez.postulacionautentia.Activities.Activities.Classes.Huellero;
import com.example.guatadepez.postulacionautentia.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DetailActivity extends AppCompatActivity {

    String url= "http:// qa.autentia.cl/test_biometria_movil.php/getFpDescriptions";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        final String uid = getIntent().getStringExtra("uid");

        Log.e("uid",uid);
        callqueue(uid);

    }

    void callqueue(final String uid){

        final String uid_local = uid;

        RequestQueue queue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        Log.e("response", String.valueOf(response));

                        for(int i=0;i<response.length();i++){

                            // Aqui no me devolvió nunca información con el parametro UID que sale en la hoja de instrucciones, por lo tanto no pude hacer la ultima pantalla con los detalles.
                            //probé con el id de cada huellero y con el uid global y no pasó nada.
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "4f206d3636e3fa50322b1a89dd50805bf8f056f5");

                return params;
            }
            @Override
            public Map<String, String> getParams() throws AuthFailureError{
                Map<String, String> parameter = new HashMap<>();
                parameter.put("Uid", uid);
                return parameter;

            }
        };

        queue.add(jsonArrayRequest);

        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<JsonRequest>() {
            @Override
            public void onRequestFinished(Request<JsonRequest> request) {


                //mAdapter.notifyDataSetChanged();
            }
        });

    }
}
