package com.example.guatadepez.postulacionautentia.Activities.Activities;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.example.guatadepez.postulacionautentia.Activities.Activities.Adapters.HuellerosAdapter;
import com.example.guatadepez.postulacionautentia.Activities.Activities.Classes.Huellero;
import com.example.guatadepez.postulacionautentia.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HuellerosActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private HuellerosAdapter mAdapter;
    private List<Huellero> huelleroList = new ArrayList<>();
    String url= "http://qa.autentia.cl/test_biometria_movil.php/getFpMachines/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_huelleros);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        final String uid = getIntent().getStringExtra("uid");

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new HuellerosAdapter(this,huelleroList,uid);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        Log.e("uid",uid);

        callqueue(uid);


    }

    void callqueue(final String uid){

        final String uid_local = uid;

        RequestQueue queue = Volley.newRequestQueue(this);


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for(int i=0;i<response.length();i++){
                            // Get current json object
                            JSONObject huelleroArray = null;
                            JSONObject huellerosubarray = null;
                            try {
                                huelleroArray = response.getJSONObject(i);
                                huellerosubarray = response.getJSONObject(i).getJSONObject("desc");
                                Huellero huellero = new Huellero(huelleroArray.getString("name"),
                                        huellerosubarray.getString("vendor_id"),
                                        huellerosubarray.getString("product_id"),
                                        huelleroArray.getString("url"));
                                huelleroList.add(huellero);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "4f206d3636e3fa50322b1a89dd50805bf8f056f5");

                return params;
            }
            @Override
            public Map<String, String> getParams() throws AuthFailureError{
                Map<String, String> parameter = new HashMap<>();
                parameter.put("Uid", uid_local);
                return parameter;

            }
        };

        queue.add(jsonArrayRequest);

        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<JsonRequest>() {
            @Override
            public void onRequestFinished(Request<JsonRequest> request) {


                mAdapter.notifyDataSetChanged();
            }
        });

    }
}
